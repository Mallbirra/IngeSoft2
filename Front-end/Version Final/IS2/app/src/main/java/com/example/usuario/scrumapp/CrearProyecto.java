package com.example.usuario.scrumapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

public class CrearProyecto extends AppCompatActivity {

    private Button btnResgistrar;
    private EditText name, fechaInicio, fechaFin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_proyecto);
        btnResgistrar = (Button) findViewById(R.id.btnRegistrarProyecto);
        name = (EditText) findViewById(R.id.textName);
        fechaInicio = (EditText) findViewById(R.id.textInicio);
        fechaFin = (EditText) findViewById(R.id.textFin);

        btnResgistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    registrarProyecto();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    @SuppressLint("WrongConstant")
    private void registrarProyecto() throws JSONException {

        //   HashMap<String, String> parameters = new HashMap<String, String>();

        JSONObject parameters = new JSONObject();
        boolean registrado = false;
        String fechaComienzo = fechaInicio.getText().toString()+"T00:00:00-04:00";
        String fechaFinalizacion = fechaFin.getText().toString()+"T00:00:00-04:00";
        parameters.put("nombreProyecto", name.getText().toString());
        parameters.put("fechaInicio", fechaComienzo);
        parameters.put("fechaFinEstimada", fechaFinalizacion);
        parameters.put("estado", "ACT");
        parameters.put("anho", Integer.parseInt(fechaComienzo.substring(0, 4)));

        registrado = ConexionManager.executePost(Configuracion.URL_BASE_SERVICE+"org.postgres.entities.proyectos/RegistrarProyecto", parameters);

        if(registrado){
            Intent intent = new Intent(getApplicationContext(), ListarProyectos.class);
            startActivity(intent);
        }
    }


}
