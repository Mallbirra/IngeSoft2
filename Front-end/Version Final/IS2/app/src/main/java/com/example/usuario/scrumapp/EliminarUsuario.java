package com.example.usuario.scrumapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class EliminarUsuario extends AppCompatActivity {

    private ListView listUsers;
    private JSONArray lista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eliminar_usuario);
        listUsers = (ListView) findViewById(R.id.listUsers);

        String result = ConexionManager.executeGet(Configuracion.URL_BASE_SERVICE+"org.postgres.entities.usuarios");

        try {
            JSONArray jArray = new JSONArray(result);
            JSONObject jo = new JSONObject();
            jo.put("arrayName",jArray);
            System.out.println("aaa"+jArray);
            jsonConvertir(jo);
           // String textItemList = (listUsers.getItemAtPosition(position));
           listUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    try {

                        JSONObject usuarioSeleccionado = lista.getJSONObject(position);
                        String idUsuario = usuarioSeleccionado.getString("idUsuario");
                        System.out.println(idUsuario);
                        ConexionManager.executePut(Configuracion.URL_BASE_SERVICE+"org.postgres.entities.usuarios/elimin/"+Integer.parseInt(idUsuario));
//PONER MENSAJE SE HA ELIMINADO CORRECTAMENTE AL USUARIO
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Parseo de la respuesta al componente ListView
    private void jsonConvertir(JSONObject obj){
        try{
            System.out.println("Entra a la funcion");
            List<String> contes = new ArrayList<String>();
            System.out.println("obj "+obj);
            lista = obj.optJSONArray("arrayName");
            System.out.println("JSONArray lista: "+lista);
            for(int i = 0; i < lista.length(); i++){
                JSONObject json_data = lista.getJSONObject(i);
                System.out.println("JSONObject json_data: "+json_data);
                String conte = json_data.getString("idUsuario") + " " + json_data.getString("nombre") + " " + json_data.getString("apellido")+ " " +
                        json_data.getString("email") + " " + json_data.getString("telefono") + " " + json_data.getString("estado");
                contes.add(conte);
                System.out.println("conte :"+ contes);
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contes);
            listUsers.setAdapter(adapter);
        } catch (JSONException e) {
            Toast.makeText(this, "Error al cargar la lista:" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }finally {

        }
    }
}
