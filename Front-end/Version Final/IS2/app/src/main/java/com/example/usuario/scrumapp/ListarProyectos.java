package com.example.usuario.scrumapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ListarProyectos extends AppCompatActivity {

    private ListView listProyectos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_proyectos);

        listProyectos = (ListView) findViewById(R.id.listProject);

        String result = ConexionManager.executeGet(Configuracion.URL_BASE_SERVICE+"org.postgres.entities.proyectos");

        try {
            JSONArray jArray = new JSONArray(result);
            JSONObject jo = new JSONObject();
            jo.put("arrayName",jArray);
            System.out.println("aaa"+jArray);
            jsonConvertir(jo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    //Parseo de la respuesta al componente ListView
    private void jsonConvertir(JSONObject obj){
        try{
            System.out.println("Entra a la funcion");
            List<String> contes = new ArrayList<String>();
            System.out.println("obj "+obj);
            JSONArray lista = obj.optJSONArray("arrayName");
            System.out.println("JSONArray lista: "+lista);
            for(int i = 0; i < lista.length(); i++){
                JSONObject json_data = lista.getJSONObject(i);
                System.out.println("JSONObject json_data: "+json_data);
                String estado = json_data.getString("estado");

                String conte = json_data.getString("nombreProyecto") + " " + json_data.getString("estado")+ " " +
                        json_data.getString("fechaInicio") + " " + json_data.getString("fechaFinEstimada");
                contes.add(conte);
                System.out.println("conte :"+ contes);
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contes);
            listProyectos.setAdapter(adapter);
        } catch (JSONException e) {
            Toast.makeText(this, "Error al cargar la lista:" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }finally {

        }
    }


}
