package com.example.usuario.scrumapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

public class CrearUs extends AppCompatActivity {

  /*  @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_us);
    }*/

    private Button btnResgistrar;
    private EditText Duracion, NombreCorto, NombreLargo, ValorDeNegocio, ValorTecnico, Descripcion,Prioridad,Fecha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_us);
        btnResgistrar = (Button) findViewById(R.id.btnRegistrarUS);
        Duracion = (EditText) findViewById(R.id.textDuracion);
        NombreCorto = (EditText) findViewById(R.id.textNombreCorto);
        NombreLargo = (EditText) findViewById(R.id.textNombreLargo);
        ValorDeNegocio = (EditText) findViewById(R.id.textValorNe);
        ValorTecnico = (EditText) findViewById(R.id.TextValorTec);
        Descripcion = (EditText) findViewById(R.id.textDescrip);
        Prioridad = (EditText) findViewById(R.id.textPrioridad);
        Fecha = (EditText) findViewById(R.id.textFecha);

        btnResgistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    registrarUS();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @SuppressLint("WrongConstant")
    private void registrarUS() throws JSONException {

        //   HashMap<String, String> parameters = new HashMap<String, String>();

        JSONObject parameters = new JSONObject();
        Integer duracionUs =  Integer.parseInt(Duracion.getText().toString());
        Integer valorNego = Integer.parseInt(ValorDeNegocio.getText().toString());
        Integer valorTe = Integer.parseInt(ValorTecnico.getText().toString());
        boolean registrado = false;
        parameters.put("idSprint",0);
        parameters.put("duracion", duracionUs);
        parameters.put("nombreCorto", NombreCorto.getText().toString());
        parameters.put("nombreLargo", NombreLargo.getText().toString());
        parameters.put("valorDelNegocio", valorNego);
        parameters.put("valorTecnico", valorTe);
        parameters.put("descripcion", Descripcion.getText().toString());
        parameters.put("tiempoDeTrabajo", 0);
        parameters.put("estado", "PEN");
        parameters.put("prioridad", Prioridad.getText().toString());
        parameters.put("fechaFin", Fecha.getText().toString()+"T00:00:00-04:00");

        registrado = ConexionManager.executePost(Configuracion.URL_BASE_SERVICE+"org.postgres.entities.userstories", parameters);

        if(registrado){
            Intent intent = new Intent(getApplicationContext(), ListarTareas.class);
            startActivity(intent);
        }
    }
}

