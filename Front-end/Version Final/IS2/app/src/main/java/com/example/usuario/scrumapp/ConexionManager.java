package com.example.usuario.scrumapp;

import android.annotation.SuppressLint;
import android.os.StrictMode;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ConexionManager {
    @SuppressLint("WrongConstant")
    public static String executeGet(String targetURL) {
        int timeout = 5000;
        String result;
        String inputLine;
        URL url;
        HttpURLConnection connection = null;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //Se realiza la conexion
        try {
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();

            System.out.println("   CONEXIÓN!!" + connection);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
               /*

                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setConnectTimeout(timeout);
                connection.setReadTimeout(timeout);*/

            int status = connection.getResponseCode();
            System.out.println("estado!! " + status);
            connection.connect();
            //Create a new InputStreamReader
            InputStreamReader streamReader = new
                    InputStreamReader(connection.getInputStream());
            //Create a new buffered reader and String Builder
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();
            //Check if the line we are reading is not null
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
            //Close our InputStream and Buffered reader
            reader.close();
            streamReader.close();
            //Set our result equal to our stringBuilder
            result = stringBuilder.toString();
            System.out.println("resultttt " + result);
            return result;

        } catch (Exception e) {
            //Toast.makeText(R.layout.activity_main, "Error de conexión", 10).show();
            e.printStackTrace();
        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }

    @SuppressLint("WrongConstant")
    public static boolean executePost(String targetURL, JSONObject parameters) {

        String response = "";
        int timeout = 5000;
        String inputLine;
        String result, urlParameters;
        URL url;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        HttpURLConnection connection = null;
        //Se intenta hacer la conexion
        try {
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);

            // Se envia la peticion al servidor añadiendole los parametros
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());

            wr.writeBytes(parameters.toString());
            wr.flush();
            wr.close();
            System.out.println(connection.getResponseCode());
            System.out.println(connection.getResponseMessage());
            //Obtenemos la respuesta del servidor
            InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());
            //Create a new buffered reader and String Builder
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();
            //Check if the line we are reading is not null
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
            //Close our InputStream and Buffered reader
            reader.close();
            streamReader.close();
            //Set our result equal to our stringBuilder
            result = stringBuilder.toString();
            System.out.println("resultttt " + result);
            return true;

        } catch (Exception e) {
            // Toast.makeText(this,"Error de conexión", 10).show();
            e.printStackTrace();
        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
        return false;
    }

    /*Put que recibe id como parámetro*/
    @SuppressLint("WrongConstant")
    public static void executePut(String targetURL){
        String response = "";
        int timeout = 5000;
        String inputLine;
        String result, urlParameters;
        URL url;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        HttpURLConnection connection = null;
        //Se intenta hacer la conexion
        try {
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("PUT");
            connection.setRequestProperty("Content-Type", "application/json");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);

            System.out.println(connection.getResponseCode());
            System.out.println(connection.getResponseMessage());
            //Obtenemos la respuesta del servidor
            InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());
            //Create a new buffered reader and String Builder
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();
            //Check if the line we are reading is not null
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
            //Close our InputStream and Buffered reader
            reader.close();
            streamReader.close();
            //Set our result equal to our stringBuilder
            result = stringBuilder.toString();
            System.out.println("resultttt " + result);

        } catch (Exception e) {
            // Toast.makeText(this,"Error de conexión", 10).show();
            e.printStackTrace();
        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }

    }

    //PUT que recibe todos los datos
    @SuppressLint("WrongConstant")
    public static void executePutDatos(String targetURL, JSONObject parameters) {
        String response = "";
        int timeout = 5000;
        String inputLine;
        String result, urlParameters;
        URL url;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        HttpURLConnection connection = null;
        //Se intenta hacer la conexion
        try {
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("PUT");
            connection.setRequestProperty("Content-Type", "application/json");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);

            // Se envia la peticion al servidor añadiendole los parametros
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());

            wr.writeBytes(parameters.toString());
            wr.flush();
            wr.close();
            System.out.println(connection.getResponseCode());
            System.out.println(connection.getResponseMessage());
            //Obtenemos la respuesta del servidor
            InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());
            //Create a new buffered reader and String Builder
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();
            //Check if the line we are reading is not null
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
            //Close our InputStream and Buffered reader
            reader.close();
            streamReader.close();
            //Set our result equal to our stringBuilder
            result = stringBuilder.toString();
            System.out.println("resultttt " + result);

        } catch (Exception e) {
            // Toast.makeText(this,"Error de conexión", 10).show();
            e.printStackTrace();
        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    /*Metodo que realiza la conexion con el servidor y solicita el servicio post del login*/
    @SuppressLint("WrongConstant")
    public static String executePostString(String targetURL, String urlParameters) {
        int timeout=5000;
        String inputLine;
        String result;
        URL url;
        HttpURLConnection connection = null;
        //Se intenta hacer la conexion
        try {
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);

            // Se envia la peticion al servidor añadiendole los parametros
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
            System.out.println(connection.getResponseCode());
            System.out.println(connection.getResponseMessage());
            //Obtenemos la respuesta del servidor
            InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());
            //Create a new buffered reader and String Builder
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();
            //Check if the line we are reading is not null
            while((inputLine = reader.readLine()) != null){
                stringBuilder.append(inputLine);
            }
            //Close our InputStream and Buffered reader
            reader.close();
            streamReader.close();
            //Set our result equal to our stringBuilder
            result = stringBuilder.toString();
            System.out.println("resultttt "+result);
            return result;

        } catch (Exception e) {
          //  Toast.makeText(this,"Error de conexión", 10).show();
            e.printStackTrace();
        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }

}

