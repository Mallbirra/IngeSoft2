package com.example.usuario.scrumapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

public class ActualizarUsuario extends AppCompatActivity {

    private Button btnGuardar;
    private EditText name, lastName, email, telephone, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar_usuario);

        name = (EditText) findViewById(R.id.textName);
        lastName = (EditText) findViewById(R.id.textLastName);
        email = (EditText) findViewById(R.id.textEmail);
        telephone = (EditText) findViewById(R.id.textCelular);
        password = (EditText) findViewById(R.id.textPass);

        name.setText(getIntent().getStringExtra("nombre"));
        String apellido = getIntent().getStringExtra("apellido");
        lastName.setText(getIntent().getStringExtra("apellido"));
        email.setText(getIntent().getStringExtra("email"));
        String te = getIntent().getStringExtra("telephone");
        telephone.setText(getIntent().getStringExtra("telephone"));
        password.setText(getIntent().getStringExtra("contrasenha"));
        btnGuardar= (Button)findViewById(R.id.btnActualizarUsuario);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    actualizarUsuario();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @SuppressLint("WrongConstant")
    private void actualizarUsuario() throws JSONException {

        //   HashMap<String, String> parameters = new HashMap<String, String>();

        JSONObject parameters = new JSONObject();

        parameters.put("nombre", name.getText().toString());
        parameters.put("apellido", lastName.getText().toString());
        parameters.put("email", email.getText().toString());
        parameters.put("contrasenha", password.getText().toString());
        parameters.put("telefono", telephone.getText().toString());
        parameters.put("estado", 1);
        parameters.put("detalleEstado", "ninguno");
        parameters.put("root", 0);

        ConexionManager.executePutDatos(Configuracion.URL_BASE_SERVICE+"org.postgres.entities.usuarios/"+ModificarUsuarios.ModiID, parameters);
    }
}
