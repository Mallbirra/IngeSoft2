package com.example.usuario.scrumapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class MenuScrumMaster extends AppCompatActivity {
    private Button btnUs, btnRegistrarUs, btnSprint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_scrum_master);
    }

    @SuppressLint("WrongConstant")
    public void obtenerTareas(View v) {
        btnUs = (Button)findViewById(R.id.btnMenuUs);

        btnUs.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ListarTareas.class);
                startActivity(intent);
            }
        });
    }

    @SuppressLint("WrongConstant")
    public void crearUs(View v) {
        btnRegistrarUs = (Button)findViewById(R.id.btnCrearUs);

        btnRegistrarUs.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CrearUs.class);
                startActivity(intent);
            }
        });
    }

    @SuppressLint("WrongConstant")
    public void generarSprint(View v) {
        btnSprint = (Button)findViewById(R.id.btnSprint);

        btnSprint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                JSONObject json = new JSONObject();
                try {
                    json.put("duracion", 7);
                    json.put("tiempoTrabajado", 0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Boolean result = ConexionManager.executePost(Configuracion.URL_BASE_SERVICE+"org.postgres.entities.sprints/CrearSprint/"+MainActivity.idPro, json);
                if(result){
                   // Toast.makeText(this,"No se pudo actualizar la tarea", 5).show();
                }

            }
        });
    }
}
