package com.example.usuario.scrumapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ListarTareas extends AppCompatActivity {

    private ListView list1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_tareas);
        list1 = (ListView) findViewById(R.id.listaTareas);

        String result = ConexionManager.executeGet(Configuracion.URL_BASE_SERVICE+"org.postgres.entities.usuarios/listarTareas/"+MainActivity.idUsu);

        try {
            JSONArray jArray = new JSONArray(result);
            JSONObject jo = new JSONObject();
            jo.put("arrayName",jArray);
            System.out.println("aaa"+jArray);
            jsonConvertir(jo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void jsonConvertir(JSONObject obj){
        try{
            System.out.println("Entra a la funcion");
            List<String> contes = new ArrayList<String>();
            //JSONObject obj = new JSONObject(msgJson);
            System.out.println("obj "+obj);
            JSONArray lista = obj.optJSONArray("arrayName");
            System.out.println("JSONArray lista: "+lista);
            for(int i = 0; i < lista.length(); i++){
                JSONObject json_data = lista.getJSONObject(i);
                System.out.println("JSONObject json_data: "+json_data);
                String conte = "Descripcion:"+ json_data.getString("descripcion") + "\nProyecto:" + json_data.getString("Proyecto")+ "\nTarea:" +
                        json_data.getString("idTarea") + "\nEstado:" + json_data.getString("Estado");
                contes.add(conte);
                System.out.println("conte :"+ contes);
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contes);
            list1.setAdapter(adapter);
        } catch (JSONException e) {
            Toast.makeText(this, "Error al cargar la lista:" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }finally {

        }
    }

}
