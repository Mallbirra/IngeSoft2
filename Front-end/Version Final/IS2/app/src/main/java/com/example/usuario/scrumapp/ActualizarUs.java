package com.example.usuario.scrumapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class ActualizarUs extends AppCompatActivity {
    private Button btnResgistrar;
    private EditText IdUserStorie, Duracion, NombreCorto, NombreLargo, ValorDeNegocio,
            ValorTecnico, Descripcion,Prioridad, Fecha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar_us);

        btnResgistrar = (Button) findViewById(R.id.btnRegistrarUS);
        Duracion = (EditText) findViewById(R.id.textDuracion);
        NombreCorto = (EditText) findViewById(R.id.textNombreCorto);
        NombreLargo = (EditText) findViewById(R.id.textNombreLargo);
        ValorDeNegocio = (EditText) findViewById(R.id.textValorNe);
        ValorTecnico = (EditText) findViewById(R.id.TextValorTec);
        Descripcion = (EditText) findViewById(R.id.textDescrip);
        Prioridad = (EditText) findViewById(R.id.textPrioridad);
        Fecha = (EditText) findViewById(R.id.textFecha);

        btnResgistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    actualizarUS();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }
    @SuppressLint("WrongConstant")
    private void actualizarUS() throws JSONException {

        //   HashMap<String, String> parameters = new HashMap<String, String>();

        JSONObject parameters = new JSONObject();
        Integer duracionUs =  Integer.parseInt(Duracion.getText().toString());
        Integer valorNego = Integer.parseInt(ValorDeNegocio.getText().toString());
        Integer valorTe = Integer.parseInt(ValorTecnico.getText().toString());
        Integer tiempoTrabajo = Integer.parseInt(Fecha.getText().toString());
        boolean registrado = false;

        parameters.put("duracion", duracionUs);
        parameters.put("nombreCorto", NombreCorto.getText().toString());
        parameters.put("nombreLargo", NombreLargo.getText().toString());
        parameters.put("valorDelNegocio", valorNego);
        parameters.put("valorTecnico", valorTe);
        parameters.put("descripcion", Descripcion.getText().toString());
        parameters.put("tiempoDeTrabajo", 0);
        parameters.put("estado", "ACT");
        parameters.put("prioridad", Prioridad.getText().toString());
        parameters.put("fechaFin", tiempoTrabajo);

        registrado = ConexionManager.executePost(Configuracion.URL_BASE_SERVICE+"org.postgres.entities.userstories/"+ModificarUs.IdUserStorieSelect+"/"+MainActivity.idUsu, parameters);
        if(registrado){

            Duracion.setText("");
            NombreLargo.setText("");
            NombreCorto.setText("");
            Prioridad.setText("");
            ValorDeNegocio.setText("");
            ValorTecnico.setText("");
            Descripcion.setText("");
            Fecha.setText("");

            Intent intent = new Intent(getApplicationContext(), ListarTareas.class);
            startActivity(intent);
        }
        else{
            Toast.makeText(this,"No se pudo actualizar la tarea", 5).show();
        }
    }
}

