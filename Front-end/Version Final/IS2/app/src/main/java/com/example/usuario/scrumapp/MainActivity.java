package com.example.usuario.scrumapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.scrumis2.MESSAGE";
    public static  String  idUsu, idPro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @SuppressLint("WrongConstant")
    public void verificarUsuario(View view) {

        //EditText Usuario y EditText Contraseña
        EditText editTextEmail = findViewById(R.id.txtEmail);
        EditText editTextPass = (EditText) findViewById(R.id.txtPass);

        String message;
        //Crea un objeto JSON que envia la petición
        JSONObject loginParams = new JSONObject();

        try {
            loginParams.put("email", editTextEmail.getText().toString());
            loginParams.put("contrasenha", editTextPass.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            message = ConexionManager.executePostString(Configuracion.URL_BASE_SERVICE+"org.postgres.entities.usuarios/login", loginParams.toString());
            JSONObject obj = (new JSONObject(message));
            idUsu = obj.getString("idUsuario");
            idPro = obj.getString("proyecto");

            System.out.println(loginParams.toString());
            System.out.println(message);
            if (message.equals(" ")){
                Toast.makeText(this,"Login sin éxito", 5).show();
                return;
            }else if(Integer.parseInt(obj.getString("root")) == 1){
                Intent intent = new Intent(this, Menu.class);
                startActivity(intent);
            }else if(obj.getString("rol") == "TEAM"){
                Intent intent = new Intent(this, MenuUsuarios.class);
                startActivity(intent);
            }else
            {
                Intent intent = new Intent(this, MenuScrumMaster.class);
                startActivity(intent);
            }
            // intent.putExtra(EXTRA_MESSAGE, message);

        }
        catch(NullPointerException e){
            Toast.makeText(this,"Hubo un error, no se pudo conectar con el servidor", 5).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
