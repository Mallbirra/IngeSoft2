package com.example.usuario.scrumapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ModificarUs extends AppCompatActivity {

    private ListView listUs;
    private JSONArray lista;
    public static String IdUserStorieSelect;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_us);
        listUs = (ListView) findViewById(R.id.listUserStories);

        String result = ConexionManager.executeGet(Configuracion.URL_BASE_SERVICE + "org.postgres.entities.userstories/listarTareas/"+MainActivity.idUsu);
        try {
            JSONArray jArray = new JSONArray(result);
            JSONObject jo = new JSONObject();
            jo.put("arrayName", jArray);
            System.out.println("aaa" + jArray);
            jsonConvertir(jo);

            listUs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    JSONObject usSeleccionado = null;
                    try {
                        usSeleccionado = lista.getJSONObject(position);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Intent intent = new Intent(getApplicationContext(), ActualizarUs.class);
                    try {
                        IdUserStorieSelect = usSeleccionado.getString("idUserstorie");
                        intent.putExtra("duracion", usSeleccionado.getString("duracion"));
                        intent.putExtra("nombreCorto", usSeleccionado.getString("nombreCorto"));
                        intent.putExtra("nombreLargo", usSeleccionado.getString("nombreLargo"));
                        intent.putExtra("valorDelNegocio", usSeleccionado.getString("valorDelNegocio"));
                        intent.putExtra("valorTecnico", usSeleccionado.getString("valorTecnico"));
                        intent.putExtra("descripcion", usSeleccionado.getString("descripcion"));
                        intent.putExtra("tiempoDeTrabajo", usSeleccionado.getString("tiempoDeTrabajo"));
                        intent.putExtra("estado", usSeleccionado.getString("estado"));
                        intent.putExtra("prioridad", usSeleccionado.getString("prioridad"));
                        intent.putExtra("fechaFin", usSeleccionado.getString("fechaFin"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    startActivity(intent);
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Parseo de la respuesta al componente ListView
    private void jsonConvertir(JSONObject obj) {
        try {
            System.out.println("Entra a la funcion");
            List<String> contes = new ArrayList<String>();
            System.out.println("obj " + obj);
            lista = obj.optJSONArray("arrayName");
            System.out.println("JSONArray lista: " + lista);
            for (int i = 0; i < lista.length(); i++) {
                JSONObject json_data = lista.getJSONObject(i);
                System.out.println("JSONObject json_data: " + json_data);
                String estado = json_data.getString("estado");

                if (estado == "1") {
                    estado = "Activo";
                } else {
                    estado = "Inactivo";
                }
                String conte = json_data.getString("duracion") + " " + json_data.getString("nombreCorto") + " " +
                        json_data.getString("nombreLargo") + " " + json_data.getString("valorDelNegocio") + " "
                        + json_data.getString("descripcion") + " " + json_data.getString("tiempoDeTrabajo")
                        + " " + json_data.getString("estado") + " " + json_data.getString("prioridad");
                contes.add(conte);
                System.out.println("conte :" + contes);
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contes);
            listUs.setAdapter(adapter);
        } catch (JSONException e) {
            Toast.makeText(this, "Error al cargar la lista:" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        } finally {

        }
    }
}
