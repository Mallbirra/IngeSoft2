package com.example.usuario.scrumapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuUsuarios extends AppCompatActivity {
    private Button btnUs, btnObtenerKanban;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_usuarios);
    }


    @SuppressLint("WrongConstant")
    public void obtenerTareas(View v) {
        btnUs = (Button)findViewById(R.id.btnTareasUser);

        btnUs.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ListarTareas.class);
                startActivity(intent);
            }
        });
    }

    @SuppressLint("WrongConstant")
    public void obtenerKanban(View v) {
        btnObtenerKanban = (Button)findViewById(R.id.btnKanban);

        btnObtenerKanban.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ListarKanban.class);
                startActivity(intent);
            }
        });
    }
}
