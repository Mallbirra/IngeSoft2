package com.example.usuario.scrumapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Menu extends AppCompatActivity {

    private Button btnUsuarios, btnProyectos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    @SuppressLint("WrongConstant")
    public void obtenerUsuarios(View v) {
        btnUsuarios = (Button)findViewById(R.id.btnMenuUsers);

        btnUsuarios.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Usuarios.class);
                startActivity(intent);
            }
        });
    }

    @SuppressLint("WrongConstant")
    public void obtenerProyectos(View v) {
        btnProyectos = (Button)findViewById(R.id.btnMenuProyectos);

        btnProyectos.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Proyectos.class);
                startActivity(intent);
            }
        });
    }



}
