package com.example.scrumis2;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.scrumis2.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //Método invocado cuando el usuario hace click en el botón de Login

    @SuppressLint("WrongConstant")
    public void verificarInicio(View view) {
        Intent intent;
        intent = new Intent(this, DisplayMessageActivity.class);
        //EditText Usuario y EditText Contraseña
        EditText editText = (EditText) findViewById(R.id.editText);
        //EditText editText2 = (EditText) findViewById(R.id.editText2);

        String message;
        //Crea un objeto JSON que envia la petición
        JSONObject loginParams = new JSONObject();

        try {
            loginParams.put("email", editText.getText().toString());
            //loginParams.put("contrasenha", editText2.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            message = executePost("http://192.168.172.1:8080/ingesoft2/webresources/org.postgres.entities.usuarios/login", loginParams.toString());
            if (message.equals("")){
                Toast.makeText(this,"Login sin éxito", 5).show();
                return;
            }
            intent.putExtra(EXTRA_MESSAGE, message);
            startActivity(intent);
        }
        catch(NullPointerException e){
            Toast.makeText(this,"Hubo un error, no se pudo conectar con el servidor", 5).show();
        }


    }

    /*Metodo que realiza la conexion con el servidor y solicita el servicio post del login*/
    @SuppressLint("WrongConstant")
    public String executePost(String targetURL, String urlParameters) {
        int timeout=5000;
        URL url;
        HttpURLConnection connection = null;
        //Se intenta hacer la conexion
        try {
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);

            // Se envia la peticion
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            // Obtiene resultados de la peticion realizada
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();

        } catch (Exception e) {
            Toast.makeText(this,"Error de conexión", 10).show();
            e.printStackTrace();
        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }
}

