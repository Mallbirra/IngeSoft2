package org.postgres.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.postgres.entities.Permisos;
import org.postgres.entities.UsuariosProyectos;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-08-31T20:37:43")
@StaticMetamodel(Roles.class)
public class Roles_ { 

    public static volatile SingularAttribute<Roles, String> descripcion;
    public static volatile SingularAttribute<Roles, Integer> idRol;
    public static volatile CollectionAttribute<Roles, Permisos> permisosCollection;
    public static volatile SingularAttribute<Roles, String> nombre;
    public static volatile CollectionAttribute<Roles, UsuariosProyectos> usuariosProyectosCollection;

}