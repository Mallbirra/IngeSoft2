package org.postgres.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.postgres.entities.Tareas;
import org.postgres.entities.Usuarios;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-08-31T20:37:43")
@StaticMetamodel(UsuariosTareas.class)
public class UsuariosTareas_ { 

    public static volatile SingularAttribute<UsuariosTareas, Date> fechaCambio;
    public static volatile SingularAttribute<UsuariosTareas, Integer> idUsuarioTarea;
    public static volatile SingularAttribute<UsuariosTareas, Tareas> idTarea;
    public static volatile SingularAttribute<UsuariosTareas, Usuarios> idUsuario;
    public static volatile SingularAttribute<UsuariosTareas, String> detalleCambio;

}