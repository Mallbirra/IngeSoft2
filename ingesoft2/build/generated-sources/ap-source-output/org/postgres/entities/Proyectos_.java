package org.postgres.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.postgres.entities.Sprints;
import org.postgres.entities.UsuariosProyectos;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-08-31T20:37:43")
@StaticMetamodel(Proyectos.class)
public class Proyectos_ { 

    public static volatile SingularAttribute<Proyectos, String> descripcion;
    public static volatile SingularAttribute<Proyectos, Integer> idProyecto;
    public static volatile SingularAttribute<Proyectos, String> estado;
    public static volatile CollectionAttribute<Proyectos, Sprints> sprintsCollection;
    public static volatile SingularAttribute<Proyectos, String> nombre;
    public static volatile CollectionAttribute<Proyectos, UsuariosProyectos> usuariosProyectosCollection;

}