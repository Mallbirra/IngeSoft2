package org.postgres.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.postgres.entities.Proyectos;
import org.postgres.entities.Roles;
import org.postgres.entities.Usuarios;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-08-31T20:37:43")
@StaticMetamodel(UsuariosProyectos.class)
public class UsuariosProyectos_ { 

    public static volatile SingularAttribute<UsuariosProyectos, Proyectos> idProyecto;
    public static volatile SingularAttribute<UsuariosProyectos, Roles> idRol;
    public static volatile SingularAttribute<UsuariosProyectos, Usuarios> idUsuario;
    public static volatile SingularAttribute<UsuariosProyectos, Integer> idUsusairoProyecto;

}