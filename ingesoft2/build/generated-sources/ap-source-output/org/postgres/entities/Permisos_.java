package org.postgres.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.postgres.entities.Roles;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-08-31T20:37:43")
@StaticMetamodel(Permisos.class)
public class Permisos_ { 

    public static volatile SingularAttribute<Permisos, String> descripcion;
    public static volatile SingularAttribute<Permisos, Roles> idRol;
    public static volatile SingularAttribute<Permisos, Integer> idPermiso;
    public static volatile SingularAttribute<Permisos, String> nombre;

}