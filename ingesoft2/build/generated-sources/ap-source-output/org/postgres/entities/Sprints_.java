package org.postgres.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.postgres.entities.Proyectos;
import org.postgres.entities.Tareas;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-08-31T20:37:43")
@StaticMetamodel(Sprints.class)
public class Sprints_ { 

    public static volatile SingularAttribute<Sprints, Proyectos> idProyecto;
    public static volatile SingularAttribute<Sprints, String> estado;
    public static volatile CollectionAttribute<Sprints, Tareas> tareasCollection;
    public static volatile SingularAttribute<Sprints, Date> fechaInicio;
    public static volatile SingularAttribute<Sprints, Integer> idSprint;
    public static volatile SingularAttribute<Sprints, Date> fechaFin;

}