package org.postgres.entities;

import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.postgres.entities.Sprints;
import org.postgres.entities.UsuariosTareas;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-08-31T20:37:43")
@StaticMetamodel(Tareas.class)
public class Tareas_ { 

    public static volatile SingularAttribute<Tareas, String> descripcion;
    public static volatile SingularAttribute<Tareas, String> estado;
    public static volatile SingularAttribute<Tareas, BigInteger> tiempoEstimado;
    public static volatile SingularAttribute<Tareas, Date> fechaInicio;
    public static volatile SingularAttribute<Tareas, Integer> idTarea;
    public static volatile CollectionAttribute<Tareas, UsuariosTareas> usuariosTareasCollection;
    public static volatile SingularAttribute<Tareas, Sprints> idSprint;
    public static volatile SingularAttribute<Tareas, BigInteger> tiempoRestante;
    public static volatile SingularAttribute<Tareas, String> nombre;
    public static volatile SingularAttribute<Tareas, Date> fechaFin;
    public static volatile SingularAttribute<Tareas, BigInteger> prioridad;

}