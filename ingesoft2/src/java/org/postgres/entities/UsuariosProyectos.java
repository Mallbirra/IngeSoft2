/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.postgres.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "usuarios_proyectos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuariosProyectos.findAll", query = "SELECT u FROM UsuariosProyectos u")
    , @NamedQuery(name = "UsuariosProyectos.findByIdUsusairoProyecto", query = "SELECT u FROM UsuariosProyectos u WHERE u.idUsusairoProyecto = :idUsusairoProyecto")})
public class UsuariosProyectos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_ususairo_proyecto")
    private Integer idUsusairoProyecto;
    @JoinColumn(name = "id_proyecto", referencedColumnName = "id_proyecto")
    @ManyToOne(optional = false)
    private Proyectos idProyecto;
    @JoinColumn(name = "id_rol", referencedColumnName = "id_rol")
    @ManyToOne(optional = false)
    private Roles idRol;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuarios idUsuario;

    public UsuariosProyectos() {
    }

    public UsuariosProyectos(Integer idUsusairoProyecto) {
        this.idUsusairoProyecto = idUsusairoProyecto;
    }

    public Integer getIdUsusairoProyecto() {
        return idUsusairoProyecto;
    }

    public void setIdUsusairoProyecto(Integer idUsusairoProyecto) {
        this.idUsusairoProyecto = idUsusairoProyecto;
    }

    public Proyectos getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Proyectos idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Roles getIdRol() {
        return idRol;
    }

    public void setIdRol(Roles idRol) {
        this.idRol = idRol;
    }

    public Usuarios getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsusairoProyecto != null ? idUsusairoProyecto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuariosProyectos)) {
            return false;
        }
        UsuariosProyectos other = (UsuariosProyectos) object;
        if ((this.idUsusairoProyecto == null && other.idUsusairoProyecto != null) || (this.idUsusairoProyecto != null && !this.idUsusairoProyecto.equals(other.idUsusairoProyecto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.postgres.entities.UsuariosProyectos[ idUsusairoProyecto=" + idUsusairoProyecto + " ]";
    }
    
}
