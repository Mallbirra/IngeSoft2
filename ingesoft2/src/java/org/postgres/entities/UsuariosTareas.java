/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.postgres.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "usuarios_tareas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuariosTareas.findAll", query = "SELECT u FROM UsuariosTareas u")
    , @NamedQuery(name = "UsuariosTareas.findByIdUsuarioTarea", query = "SELECT u FROM UsuariosTareas u WHERE u.idUsuarioTarea = :idUsuarioTarea")
    , @NamedQuery(name = "UsuariosTareas.findByFechaCambio", query = "SELECT u FROM UsuariosTareas u WHERE u.fechaCambio = :fechaCambio")
    , @NamedQuery(name = "UsuariosTareas.findByDetalleCambio", query = "SELECT u FROM UsuariosTareas u WHERE u.detalleCambio = :detalleCambio")})
public class UsuariosTareas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_usuario_tarea")
    private Integer idUsuarioTarea;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_cambio")
    @Temporal(TemporalType.DATE)
    private Date fechaCambio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "detalle_cambio")
    private String detalleCambio;
    @JoinColumn(name = "id_tarea", referencedColumnName = "id_tarea")
    @ManyToOne(optional = false)
    private Tareas idTarea;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuarios idUsuario;

    public UsuariosTareas() {
    }

    public UsuariosTareas(Integer idUsuarioTarea) {
        this.idUsuarioTarea = idUsuarioTarea;
    }

    public UsuariosTareas(Integer idUsuarioTarea, Date fechaCambio, String detalleCambio) {
        this.idUsuarioTarea = idUsuarioTarea;
        this.fechaCambio = fechaCambio;
        this.detalleCambio = detalleCambio;
    }

    public Integer getIdUsuarioTarea() {
        return idUsuarioTarea;
    }

    public void setIdUsuarioTarea(Integer idUsuarioTarea) {
        this.idUsuarioTarea = idUsuarioTarea;
    }

    public Date getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(Date fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public String getDetalleCambio() {
        return detalleCambio;
    }

    public void setDetalleCambio(String detalleCambio) {
        this.detalleCambio = detalleCambio;
    }

    public Tareas getIdTarea() {
        return idTarea;
    }

    public void setIdTarea(Tareas idTarea) {
        this.idTarea = idTarea;
    }

    public Usuarios getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuarioTarea != null ? idUsuarioTarea.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuariosTareas)) {
            return false;
        }
        UsuariosTareas other = (UsuariosTareas) object;
        if ((this.idUsuarioTarea == null && other.idUsuarioTarea != null) || (this.idUsuarioTarea != null && !this.idUsuarioTarea.equals(other.idUsuarioTarea))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.postgres.entities.UsuariosTareas[ idUsuarioTarea=" + idUsuarioTarea + " ]";
    }
    
}
