/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.postgres.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "tareas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tareas.findAll", query = "SELECT t FROM Tareas t")
    , @NamedQuery(name = "Tareas.findByIdTarea", query = "SELECT t FROM Tareas t WHERE t.idTarea = :idTarea")
    , @NamedQuery(name = "Tareas.findByPrioridad", query = "SELECT t FROM Tareas t WHERE t.prioridad = :prioridad")
    , @NamedQuery(name = "Tareas.findByNombre", query = "SELECT t FROM Tareas t WHERE t.nombre = :nombre")
    , @NamedQuery(name = "Tareas.findByFechaFin", query = "SELECT t FROM Tareas t WHERE t.fechaFin = :fechaFin")
    , @NamedQuery(name = "Tareas.findByTiempoRestante", query = "SELECT t FROM Tareas t WHERE t.tiempoRestante = :tiempoRestante")
    , @NamedQuery(name = "Tareas.findByTiempoEstimado", query = "SELECT t FROM Tareas t WHERE t.tiempoEstimado = :tiempoEstimado")
    , @NamedQuery(name = "Tareas.findByFechaInicio", query = "SELECT t FROM Tareas t WHERE t.fechaInicio = :fechaInicio")
    , @NamedQuery(name = "Tareas.findByEstado", query = "SELECT t FROM Tareas t WHERE t.estado = :estado")
    , @NamedQuery(name = "Tareas.findByDescripcion", query = "SELECT t FROM Tareas t WHERE t.descripcion = :descripcion")})
public class Tareas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tarea")
    private Integer idTarea;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prioridad")
    private BigInteger prioridad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tiempo_restante")
    private BigInteger tiempoRestante;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tiempo_estimado")
    private BigInteger tiempoEstimado;
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "estado")
    private String estado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "id_sprint", referencedColumnName = "id_sprint")
    @ManyToOne(optional = false)
    private Sprints idSprint;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTarea")
    private Collection<UsuariosTareas> usuariosTareasCollection;

    public Tareas() {
    }

    public Tareas(Integer idTarea) {
        this.idTarea = idTarea;
    }

    public Tareas(Integer idTarea, BigInteger prioridad, String nombre, BigInteger tiempoRestante, BigInteger tiempoEstimado, String estado, String descripcion) {
        this.idTarea = idTarea;
        this.prioridad = prioridad;
        this.nombre = nombre;
        this.tiempoRestante = tiempoRestante;
        this.tiempoEstimado = tiempoEstimado;
        this.estado = estado;
        this.descripcion = descripcion;
    }

    public Integer getIdTarea() {
        return idTarea;
    }

    public void setIdTarea(Integer idTarea) {
        this.idTarea = idTarea;
    }

    public BigInteger getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(BigInteger prioridad) {
        this.prioridad = prioridad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public BigInteger getTiempoRestante() {
        return tiempoRestante;
    }

    public void setTiempoRestante(BigInteger tiempoRestante) {
        this.tiempoRestante = tiempoRestante;
    }

    public BigInteger getTiempoEstimado() {
        return tiempoEstimado;
    }

    public void setTiempoEstimado(BigInteger tiempoEstimado) {
        this.tiempoEstimado = tiempoEstimado;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Sprints getIdSprint() {
        return idSprint;
    }

    public void setIdSprint(Sprints idSprint) {
        this.idSprint = idSprint;
    }

    @XmlTransient
    public Collection<UsuariosTareas> getUsuariosTareasCollection() {
        return usuariosTareasCollection;
    }

    public void setUsuariosTareasCollection(Collection<UsuariosTareas> usuariosTareasCollection) {
        this.usuariosTareasCollection = usuariosTareasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTarea != null ? idTarea.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tareas)) {
            return false;
        }
        Tareas other = (Tareas) object;
        if ((this.idTarea == null && other.idTarea != null) || (this.idTarea != null && !this.idTarea.equals(other.idTarea))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.postgres.entities.Tareas[ idTarea=" + idTarea + " ]";
    }
    
}
